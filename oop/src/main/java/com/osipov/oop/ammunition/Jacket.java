package com.osipov.oop.ammunition;

import com.osipov.oop.attribute.Material;
import com.osipov.oop.attribute.Size;

public class Jacket extends Ammunition {
    private Material material = Material.Leather;

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Jacket{" +
                "material=" + material +
                "} " + super.toString();
    }

    public static class JacketBuilder {
        private Jacket newJacket;

        public JacketBuilder() {
            newJacket = new Jacket();
        }

        public JacketBuilder withCost(int cost) {
            newJacket.setCost(cost);
            return this;
        }

        public JacketBuilder withSize(Size size) {
            newJacket.setSize(size);
            return this;
        }

        public JacketBuilder withModel(String model) {
            newJacket.setModel(model);
            return this;
        }

        public JacketBuilder withDateOfIssue(int dateOfIssue) {
            newJacket.setDateOfIssue(dateOfIssue);
            return this;
        }

        public JacketBuilder withMaterial(Material material) {
            newJacket.setMaterial(material);
            return this;
        }

        public Jacket build() {
            return newJacket;
        }
    }
}
