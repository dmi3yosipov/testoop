package com.osipov.oop.ammunition;

import com.osipov.oop.attribute.Color;
import com.osipov.oop.attribute.Size;

public class Pants extends Ammunition {
    private Color color = Color.Black;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Pants{" +
                "color=" + color +
                "} " + super.toString();
    }

    public static class PantsBuilder {
        private Pants newPants;

        public PantsBuilder() {
            newPants = new Pants();
        }

        public PantsBuilder withCost(int cost) {
            newPants.setCost(cost);
            return this;
        }

        public PantsBuilder withSize(Size size) {
            newPants.setSize(size);
            return this;
        }

        public PantsBuilder withModel(String model) {
            newPants.setModel(model);
            return this;
        }

        public PantsBuilder withDateOfIssue(int dateOfIssue) {
            newPants.setDateOfIssue(dateOfIssue);
            return this;
        }

        public PantsBuilder withColor(Color color) {
            newPants.setColor(color);
            return this;
        }

        public Pants build() {
            return newPants;
        }
    }
}
