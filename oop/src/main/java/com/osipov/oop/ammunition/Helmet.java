package com.osipov.oop.ammunition;

import com.osipov.oop.attribute.HelmetType;
import com.osipov.oop.attribute.Size;

public class Helmet extends Ammunition {
    private HelmetType type = HelmetType.Open;

    private int numberOfShells = 1;

    public HelmetType getType() {
        return type;
    }

    public int getNumberOfShells() {
        return numberOfShells;
    }

    public void setType(HelmetType type) {
        this.type = type;
    }

    public void setNumberOfShells(int numberOfShells) {
        this.numberOfShells = numberOfShells;
    }

    @Override
    public String toString() {
        return "Helmet{" +
                "type='" + type + '\'' +
                ", numberOfShells=" + numberOfShells +
                "} " + super.toString();
    }

    public static class HelmetBuilder {
        private Helmet newHelmet;

        public HelmetBuilder() {
            newHelmet = new Helmet();
        }

        public HelmetBuilder withCost(int cost) {
            newHelmet.setCost(cost);
            return this;
        }

        public HelmetBuilder withSize(Size size) {
            newHelmet.setSize(size);
            return this;
        }

        public HelmetBuilder withModel(String model) {
            newHelmet.setModel(model);
            return this;
        }

        public HelmetBuilder withDateOfIssue(int dateOfIssue) {
            newHelmet.setDateOfIssue(dateOfIssue);
            return this;
        }

        public HelmetBuilder withType(HelmetType type) {
            newHelmet.setType(type);
            return this;
        }

        public HelmetBuilder withNumberOfShells(int numberOfShells) {
            newHelmet.setNumberOfShells(numberOfShells);
            return this;
        }

        public Helmet build() {
            return newHelmet;
        }
    }
}
