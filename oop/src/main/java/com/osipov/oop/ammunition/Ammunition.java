package com.osipov.oop.ammunition;

import com.osipov.oop.attribute.Size;

public abstract class Ammunition {
    private int cost = 100;

    private Size size = Size.XL;

    private String model = "undetermined";

    private int dateOfIssue = 2018;

    public int getCost() {
        return cost;
    }

    public Size getSize() {
        return size;
    }

    public String getModel() {
        return model;
    }

    public int getDateOfIssue() {
        return dateOfIssue;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setDateOfIssue(int dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    @Override
    public String toString() {
        return "Ammunition{" +
                "cost=" + cost +
                ", size=" + size +
                ", model='" + model + '\'' +
                ", dateOfIssue=" + dateOfIssue +
                '}';
    }
}
