package com.osipov.oop.attribute;

public enum Color {
    Blue, Black, Grey, Orange, Yellow
}
