package com.osipov.oop.attribute;

public enum Size {
    XS, S, M, L, XL, XXL
}
