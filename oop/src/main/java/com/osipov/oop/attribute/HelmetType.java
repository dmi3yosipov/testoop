package com.osipov.oop.attribute;

public enum HelmetType {
    Integral, Modular, Open, Enduro, Cross
}
