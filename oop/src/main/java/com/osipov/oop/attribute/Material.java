package com.osipov.oop.attribute;

public enum Material {
    Leather, Textile
}
