package com.osipov.oop;

import com.osipov.oop.ammunition.Ammunition;
import com.osipov.oop.ammunition.Helmet;
import com.osipov.oop.ammunition.Jacket;
import com.osipov.oop.ammunition.Pants;
import com.osipov.oop.attribute.Color;
import com.osipov.oop.attribute.HelmetType;
import com.osipov.oop.attribute.Material;
import com.osipov.oop.attribute.Size;
import com.osipov.oop.motorcyclist.Motorcyclist;

public class Main {
    public static void main(String[] args) {
        Helmet helmet = new Helmet.HelmetBuilder()
                .withCost(100)
                .withDateOfIssue(2018)
                .withNumberOfShells(2)
                .withType(HelmetType.Open)
                .withModel("LS2")
                .withSize(Size.XL)
                .build();
        Jacket jacket = new Jacket.JacketBuilder()
                .withCost(200)
                .withMaterial(Material.Leather)
                .withSize(Size.M)
                .withDateOfIssue(2017)
                .build();
        Pants pants = new Pants.PantsBuilder()
                .withCost(150)
                .withDateOfIssue(2018)
                .withSize(Size.XL)
                .withModel("Seca")
                .withColor(Color.Grey)
                .build();
        Motorcyclist motorcyclist = new Motorcyclist();
        motorcyclist
                .addItem(helmet)
                .addItem(jacket)
                .addItem(pants);
        motorcyclist.sortEquipmentByCost();
        for (Ammunition ammunition : motorcyclist.getEquipment()) {
            System.out.println(ammunition);
        }
        System.out.println("Total cost: " + motorcyclist.getTotalCost());
        System.out.println("Oldest thing: " + motorcyclist.getYearOfOldestItem());
        System.out.println("Dearest thing: " + motorcyclist.getCostOfDearestItem());
        for (Ammunition ammunition : motorcyclist.getListOfItemsInRangeBySize(Size.XL, Size.XXL)) {
            System.out.println(ammunition);
        }
    }
}
